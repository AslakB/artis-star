/**
 * @file kernel/dsde/Executive.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSDE_EXECUTIVE
#define DSDE_EXECUTIVE

#include <artis-star/kernel/dsde/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>

namespace artis {
namespace dsde {

template<class Time, class Dynamics, class Parameters, class GraphParameters>
class ExecutiveSimulator;

template<class Time, class Dynamics,
    class Parameters = common::NoParameters,
    class GraphParameters = common::NoParameters>
class ExecutiveContext
{
  typedef ExecutiveSimulator<Time, Dynamics, Parameters, GraphParameters> Simulator;

public:
  ExecutiveContext(const Parameters &parameters, const GraphParameters &graph_parameters,
                   Simulator *simulator)
      :
      _parameters(parameters), _graph_parameters(graph_parameters),
      _simulator(simulator)
  {}

  virtual ~ExecutiveContext()
  {}

  const GraphParameters &graph_parameters() const
  { return _graph_parameters; }

  const Parameters &parameters() const
  { return _parameters; }

  Simulator *simulator() const
  { return _simulator; }

private:
  const Parameters &_parameters;
  const GraphParameters &_graph_parameters;
  Simulator *_simulator;
};

template<class Time, class Dynamics,
    class Parameters = common::NoParameters,
    class GraphParameters = common::NoParameters>
class ExecutiveSimulator : public common::Simulator<Time>
{
  typedef ExecutiveSimulator<Time, Dynamics, Parameters, GraphParameters> type;

public :
  ExecutiveSimulator(const Parameters &parameters,
                     const GraphParameters &graph_parameters,
                     GraphManager<Time, Parameters, GraphParameters> &graph_manager)
      :
      common::Model<Time>("executive"),
      common::Simulator<Time>("executive"),
      _dynamics(ExecutiveContext<Time, Dynamics, Parameters, GraphParameters>(
          parameters, graph_parameters, this), graph_manager)
  {}

  ~ExecutiveSimulator()
  {}

  const Dynamics &dynamics() const
  { return _dynamics; }

  virtual void restore(const common::context::State<Time> &state)
  {
    common::Simulator<Time>::restore(state);
    _dynamics.restore(state);
  }

  virtual void save(common::context::State<Time> &state) const
  {
    common::Simulator<Time>::save(state);
    _dynamics.save(state);
  }

  virtual std::string to_string(int level) const
  {
    std::ostringstream ss;

    ss << common::String::make_spaces(level * 2) << "executive simulator \""
       << type::get_name() << "\"" << std::endl;
    return ss.str();
  }

  virtual void finish(const typename Time::type &t)
  {
#ifndef WITH_TRACE
    (void) t;
#endif

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::FINISH,
                    common::LevelType::FORMALISM);
    common::Trace<Time>::trace().flush();
#endif
  }

  typename Time::type start(const typename Time::type &t)
  {
//                When i-message(t)
//                  tl = t - e
//                  tn = tl + ta(s)
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    type::_tl = t;
    type::_tn = type::_tl + _dynamics.start(t);

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  common::Value observe(const typename Time::type &t,
                        unsigned int index) const
  { return _dynamics.observe(t, index); }

  void output(const typename Time::type &t)
  {
//                When *-message(t)
//                  if (t = tn) then
//                    y = lambda(s)
//                    send y-message(y,t) to parent
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
                    << ": BEFORE";
    common::Trace<Time>::trace().flush();
#endif

    if (t == type::_tn) {
      common::Bag<Time> bag = _dynamics.lambda(t);

      if (not bag.empty()) {
        for (auto &event : bag) {
          event.set_model(this);
        }
        dynamic_cast < common::Coordinator<Time> * >(
            type::get_parent())->dispatch_events(bag, t);
      }
    }

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
                    << ": AFTER";
    common::Trace<Time>::trace().flush();
#endif

  }

  void post_event(const typename Time::type &t,
                  const common::ExternalEvent<Time> &event)
  {

#ifndef WITH_TRACE
    (void) t;
#endif

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::POST_EVENT,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

    type::add_event(event);

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::POST_EVENT,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

  }

  typename Time::type transition(const typename Time::type &t)
  {
//                When x-message(t)
//                  if (x is empty and t = tn) then
//                    s = delta_int(s)
//                  else if (x isn't empty and t = tn)
//                    s = delta_conf(s,x)
//                  else if (x isn't empty and t < tn)
//                    e = t - tl
//                    s = delta_ext(s,e,x)
//                  tn = t + ta(s)
//                  tl = t
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    assert(type::_tl <= t and t <= type::_tn);

    if (t == type::_tn) {
      if (type::event_number() == 0) {
        _dynamics.dint(t);
      } else {
        _dynamics.dconf(t, t - type::_tl, type::get_bag());
      }
    } else {
      _dynamics.dext(t, t - type::_tl, type::get_bag());
    }
    type::_tn = t + _dynamics.ta(t);
    type::_tl = t;
    type::clear_bag();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DSDE,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = " << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

private :
  Dynamics _dynamics;
};

template<class Time, class Dyn, class Parameters = common::NoParameters, class GraphParameters = common::NoParameters>
class ExecutiveDynamics : public common::States<Time, Dyn>
{
  typedef dsde::ExecutiveSimulator<Time, Dyn, Parameters, GraphParameters> Simulator;

public:
  ExecutiveDynamics(
      const ExecutiveContext<Time, Dyn, Parameters, GraphParameters> &context)
      :
      _simulator(context.simulator()), _name("executive")
  {}

  virtual ~ExecutiveDynamics()
  {}

  virtual void dconf(typename Time::type /* t */, typename Time::type /* e */,
                     const common::Bag<Time> & /* bag */)
  {}

  virtual void dint(typename Time::type /* t */)
  {}

  virtual void dext(typename Time::type /* t */, typename Time::type /* e */,
                    const common::Bag<Time> & /* bag */)
  {}

  virtual typename Time::type
  start(typename Time::type /* time */)
  { return Time::infinity; }

  virtual typename Time::type
  ta(typename Time::type /* time */) const
  { return Time::infinity; }

  virtual common::Bag<Time>
  lambda(typename Time::type /* time */) const
  { return common::Bag<Time>(); }

  virtual common::Value observe(const typename Time::type & /* t */,
                                unsigned int /* index */) const
  { return common::Value(); }

  const std::string &get_name() const
  { return _name; }

  void restore(const common::context::State<Time> &state)
  {
    common::States<Time, Dyn>::restore(static_cast<Dyn *>(this), state);
  }

  void save(common::context::State<Time> &state) const
  {
    common::States<Time, Dyn>::save(static_cast<const Dyn *>(this), state);
  }

private:
  Simulator *_simulator;
  std::string _name;
};

template<class Time, class Dyn, class Parameters = common::NoParameters, class GraphParameters = common::NoParameters>
class Executive : public dsde::ExecutiveDynamics<Time, Dyn, Parameters, GraphParameters>
{
public:
  Executive(const ExecutiveContext<Time, Dyn, Parameters, GraphParameters> &context,
            dsde::GraphManager<Time, Parameters, GraphParameters> &graph_manager)
      :
      dsde::ExecutiveDynamics<Time, Dyn, Parameters, GraphParameters>(context),
      _graph_manager(graph_manager)
  {}

  ~Executive() override = default;

protected:
  dsde::GraphManager<Time, Parameters, GraphParameters> &graph_manager()
  { return _graph_manager; }

private:
  dsde::GraphManager<Time, Parameters, GraphParameters> &_graph_manager;
};

}
}

#endif