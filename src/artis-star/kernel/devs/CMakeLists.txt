INCLUDE_DIRECTORIES(
        ${ARTIS_BINARY_DIR}/src
        ${ARTIS_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
        ${Boost_LIBRARY_DIRS})

SET(DEVS_HPP Coordinator.hpp Dynamics.hpp GraphManager.hpp Simulator.hpp)

INSTALL(FILES ${DEVS_HPP} DESTINATION ${ARTIS_INCLUDE_DIRS}/kernel/devs)
