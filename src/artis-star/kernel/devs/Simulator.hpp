/**
 * @file kernel/devs/Simulator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVS_SIMULATOR
#define DEVS_SIMULATOR

#include <artis-star/common/Coordinator.hpp>
#include <artis-star/common/Parameters.hpp>
#include <artis-star/common/Simulator.hpp>
#include <artis-star/common/utils/String.hpp>
#include <artis-star/common/utils/Trace.hpp>

namespace artis {
namespace devs {

template<class Time, class Dynamics, class Parameters>
class Simulator;

template<class Time, class Dynamics,
    class Parameters = common::NoParameters>
class Context
{
  typedef devs::Simulator<Time, Dynamics, Parameters> Simulator;

public:
  Context(const Parameters &parameters, Simulator *simulator)
      :
      _parameters(parameters), _simulator(simulator)
  {}

  virtual ~Context()
  {}

  const Parameters &parameters() const
  { return _parameters; }

  Simulator *simulator() const
  { return _simulator; }

private:
  const Parameters &_parameters;
  Simulator *_simulator;
};

template<class Time, class Dynamics,
    class Parameters = common::NoParameters>
class Simulator : public common::Simulator<Time>
{
  typedef Simulator<Time, Dynamics, Parameters> type;

public :
  Simulator(const std::string &name, const Parameters &parameters)
      :
      common::Model<Time>(name),
      common::Simulator<Time>(name),
      _dynamics(name, Context<Time, Dynamics, Parameters>(parameters, this))
  {}

  ~Simulator() override
  {}

  const Dynamics &dynamics() const
  { return _dynamics; }

  virtual void restore(const common::context::State<Time> &state)
  {
    common::Simulator<Time>::restore(state);
    _dynamics.restore(state);
  }

  virtual void save(common::context::State<Time> &state) const
  {
    common::Simulator<Time>::save(state);
    _dynamics.save(state);
  }

  virtual std::string to_string(int level) const
  {
    std::ostringstream ss;

    ss << common::String::make_spaces(level * 2) << "p-devs simulator \""
       << type::get_name() << "\"" << std::endl;
    return ss.str();
  }

  virtual void finish(const typename Time::type &t)
  {
#ifndef WITH_TRACE
    (void) t;
#else
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::FINISH,
                    common::LevelType::FORMALISM);
    common::Trace<Time>::trace().flush();
#endif
  }

  typename Time::type start(const typename Time::type &t)
  {
//                When i-message(t)
//                  tl = t - e
//                  tn = tl + ta(s)
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    _dynamics.start(t);
    type::_tl = t;

    typename Time::type duration = _dynamics.ta(t);

    assert(duration >= 0);

    type::_tn = type::_tl + duration;

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  common::Value observe(const typename Time::type &t,
                        unsigned int index) const
  { return _dynamics.observe(t, index); }

  virtual std::string observable_name(unsigned int observable_index) const
  {
    return _dynamics.observable_name(observable_index);
  }

  void output(const typename Time::type &t)
  {
//                When *-message(t)
//                  if (t != tn) then Error
//                  y = lambda(s)
//                  send y-message(y,t) to parent coordinator
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
                    << ": BEFORE";
    common::Trace<Time>::trace().flush();
#endif

    assert(t == type::_tn);

    common::ExternalEvent<Time> event(_dynamics.lambda(t));

    if (not event.is_void()) {
      event.set_model(this);
      dynamic_cast < common::Coordinator<Time> * >(
          type::get_parent())->dispatch_events(common::Bag<Time>(event), t);
    }

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
                    << ": AFTER";
    common::Trace<Time>::trace().flush();
#endif

  }

  void post_event(const typename Time::type &t, const common::ExternalEvent<Time> &event)
  {

#ifndef WITH_TRACE
    (void) t;
#else
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::POST_EVENT,
                    common::LevelType::FORMALISM)
            << ": event = " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

    type::add_event(event);
  }

  typename Time::type transition(const typename Time::type &t)
  {
//                When x-message(t)
//                  if not (tl <= t <= tn) then Error
//                  if (x is empty and t = tn) then
//                    s = delta_int(s)
//                  else if (x isn't empty and t < tn)
//                    e = t - tl
//                    s = delta_ext(s,e,x)
//                  tl = t
//                  tn = tl + ta(s)
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    assert(type::_tl <= t and t <= type::_tn);

    if (t == type::_tn) {
      if (type::event_number() == 0) {
        _dynamics.dint(t);
      } else {
        assert(false);
      }
    } else {

      assert(type::get_bag().size() == 1);

      _dynamics.dext(t, t - type::_tl, type::get_bag().at(0));
    }
    type::_tl = t;

    typename Time::type duration = _dynamics.ta(t);

    assert(duration >= 0);

    type::_tn = type::_tl + duration;
    type::clear_bag();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DEVS,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = " << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  typename Time::type lookahead(const typename Time::type &t) const
  {
    return _dynamics.lookahead(t);
  }

private :
  Dynamics _dynamics;
};

}
} // namespace artis devs

#endif
