/**
 * @file common/utils/FormalismType.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_UTILS_FORMALISM_TYPE
#define COMMON_UTILS_FORMALISM_TYPE

#include <string>

namespace artis {
namespace common {

class FormalismType
{
public:
  enum Values
  {
    NONE = 0, DEVS, DSDE, PDEVS, QSS, DTSS, GDEVS, CELLDEVS, FDDEVS, DESS, SSS
  };

  static std::string to_string(const FormalismType::Values &s)
  {
    switch (s) {
    case NONE:return "none";
    case DEVS:return "devs";
    case DSDE:return "dsde";
    case PDEVS:return "pdevs";
    case QSS:return "qss";
    case DTSS:return "dtss";
    case GDEVS:return "gdevs";
    case CELLDEVS:return "celldevs";
    case FDDEVS:return "fddevs";
    case DESS:return "dess";
    case SSS:return "sss";
    };
    return "";
  }
};

}
}

#endif
